<?php

$settings->add(new admin_setting_configcheckbox('assignsubmission_woodenobj/default',
    new lang_string('default', 'assignsubmission_woodenobj'),
    new lang_string('default_help', 'assignsubmission_woodenobj'), 0));

    $toolsoptions = array(
        '0' => 'saw',
        '1' => 'router'
    );
    $defaulttoolsoptions = array(
    );
$settings->add(new admin_setting_configcheckbox('assignsubmission_woodenobj/default2',
    new lang_string('default', 'assignsubmission_woodenobj'),
    new lang_string('default_help', 'assignsubmission_woodenobj'), 0));



$settings->add(new admin_setting_configmultiselect('assignsubmission_woodenobj/tools',
    new lang_string('tools_title', 'assignsubmission_woodenobj'),
    new lang_string('tools_desc', 'assignsubmission_woodenobj'),
    $defaulttoolsoptions, $toolsoptions
    ));
/*
if (isset($CFG->maxbytes)) {

    $name = new lang_string('maximumsubmissionsize2', 'assignsubmission_file');
    $description = new lang_string('configmaxbytes2', 'assignsubmission_file');

    $element = new admin_setting_configselect('assignsubmission_file/maxbytes',
        $name,
        $description,
        1048576,
        get_max_upload_sizes($CFG->maxbytes));
    $settings->add($element);
}*/
