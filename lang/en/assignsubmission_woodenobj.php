<?php

$string['file'] = 'lol';
$string['pluginname'] = 'wooden creation';
$string['default'] = 'Enabled by default';
$string['technique'] = 'Mandatory technique';
$string['default_help'] = 'If set, this submission method will be enabled by default for all new assignments.';
$string['enabled'] = 'Online text';
$string['enabled_help'] = 'If enabled, students are able to type rich text directly into an editor field for their submission.';
$string['tools_desc'] = 'List of authorized tools';
$string['tools'] = 'Authorized tools';
$string['technique'] = '';
