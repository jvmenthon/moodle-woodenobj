<?php


class assign_submission_woodenobj extends assign_submission_plugin
{
    private $techniquesoptions = array(
        '0' => 'mortise tenon',
        '1' => 'dovetail'
    );
    private $toolsoptions = array(
        '0' => 'saw',
        '1' => 'router'
    );

    /**
     * Should return the name of this plugin type.
     *
     * @return string - the name
     */
    public function get_name()
    {
        return get_string('pluginname', 'assignsubmission_woodenobj');
    }

    public function get_settings(MoodleQuickForm $mform)
    {
        $name = get_string('tools', 'assignsubmission_woodenobj');
        $selectTools = $mform->addElement('select', 'assignsubmission_authorized_tools', $name, $this->toolsoptions);
        $selectTools->setMultiple(true);

        $name = get_string('technique', 'assignsubmission_woodenobj');
        $selectTools = $mform->addElement('select', 'assignsubmission_mandatory_technique', $name, $this->techniquesoptions);
        $selectTools->setMultiple(true);
    }

    public function save_settings(stdClass $data)
    {
        $this->set_config('authorized_tools', $data->assignsubmission_authorized_tools);
        $this->set_config('mandatory_technique', $data->assignsubmission_mandatory_technique);
        return true;
    }

    public function get_form_elements($submission, MoodleQuickForm $mform, stdClass $data)
    {

        $name = get_string('tools', 'assignsubmission_woodenobj');
        $selectTools = $mform->addElement('select', 'assignsubmission_authorized_tools', $name, $this->toolsoptions);
        $selectTools->setMultiple(true);


        $name = get_string('technique', 'assignsubmission_woodenobj');
        $selectTools = $mform->addElement('select', 'assignsubmission_mandatory_technique', $name, $this->techniquesoptions);
        $selectTools->setMultiple(true);

    }

    public function save(stdClass $submission, stdClass $data)
    {
        global $USER, $DB;


        $woodenobj_submission = $this->get_woodenobj_submission($submission->id);
        $used_tools = serialize($data->assignsubmission_authorized_tools);
        $used_technique = serialize($data->assignsubmission_mandatory_technique);

        if($woodenobj_submission){
            $woodenobj_submission->assignsubmission_authorized_tools = $used_tools;
            $woodenobj_submission->assignsubmission_mandatory_technique = $used_technique;
            return $DB->update_record('assignsubmission_woodenobj', $woodenobj_submission);
        }else{
            $woodenobj_submission =  new stdClass();
            $woodenobj_submission->assignsubmission_authorized_tools = $used_tools;
            $woodenobj_submission->assignsubmission_mandatory_technique = $used_technique;
            $woodenobj_submission->submission = $submission->id;
            $woodenobj_submission->assignment = $this->assignment->get_instance()->id;
            return $DB->insert_record('assignsubmission_woodenobj', $woodenobj_submission) > 0;
        }

    }

    public function view(stdClass $submission)
    {
        return $this->assignment->render_area_files('assignsubmission_file',
            ASSIGNSUBMISSION_FILE_FILEAREA,
            $submission->id);
    }

    private function get_woodenobj_submission($submissionid) {
        global $DB;
        return $DB->get_record('assignsubmission_woodenobj', array('submission'=>$submissionid));
    }
}