<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2019011400;
$plugin->requires  = 2018120301;
$plugin->component = 'assignsubmission_woodenobj';
$plugin->maturity =  MATURITY_ALPHA;
